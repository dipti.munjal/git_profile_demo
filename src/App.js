import './App.css';
import GitProfile from "./components/GitProfile/GitProfile";

function App() {
    return (
        <div className="App">
            <GitProfile/>
        </div>
    );
}

export default App;
