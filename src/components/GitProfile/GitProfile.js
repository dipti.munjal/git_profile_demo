import {useState} from "react";
import axios from "axios";

const GitProfile = () => {
    const [isLoading, setIsLoading] = useState(false)
    const [data, setData] = useState()

    async function handleButtonClick() {
        // return async () => {
        setIsLoading(true)
        setData(null)
        // await wait(2000)
        axios.get('https://api.github.com/users/dipti').then(
            (response) => {
                setData(response.data.login)
            }
        )
            .catch(error => {
                console.log(error.message)
            })
            .finally(() => setIsLoading(false))
        // };
    }

    return (
        <>
            <button onClick={handleButtonClick}>Fetch Data
            </button>

            {isLoading && <p>Loading... </p>}
            {data && <p> {data} </p>}
        </>
    )
}

export default GitProfile;