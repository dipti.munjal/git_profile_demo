import {fireEvent, render, screen, waitFor} from "@testing-library/react";
import GitProfile from './GitProfile'
import axios from "axios";

jest.mock('axios')

describe('Git Profile', () => {
    it('should render load data button', () => {
        render(<GitProfile/>)

        expect(screen.getByRole('button', {name: 'Fetch Data'})).toBeInTheDocument()
    })

    it('should show loading state when clicking on fetch data button', () => {
        axios.get.mockResolvedValue({
            data: {
                login: 'NeevUser'
            }
        })
        render(<GitProfile/>)
        const button = screen.getByRole('button', {name: 'Fetch Data'})

        fireEvent.click(button)

        expect(screen.getByText('Loading...')).toBeInTheDocument()
    })

    it('should be able to load the data when clicking on fetch data button', async () => {
        axios.get.mockResolvedValue({
            data: {
                login: 'NeevUser'
            }
        })
        render(<GitProfile/>)
        const button = screen.getByRole('button', {name: 'Fetch Data'})

        fireEvent.click(button)

        await waitFor(() => {
            expect(screen.getByText('NeevUser')).toBeInTheDocument()
        })
    })
})
